package com.app.seu.dronecontroller;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.app.AlertDialog;
import android.content.DialogInterface;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import co.ceryle.radiorealbutton.RadioRealButton;
import io.github.controlwear.virtual.joystick.android.JoystickView;

public class ControllerActivity extends AppCompatActivity {

    //Definición de los elemntos a utilizar en la aplicación
    private JoystickView leftJoystick;
    private JoystickView rightJoystick;
    private RadioRealButton rbStabilize;
    private RadioRealButton rbHold;
    private RadioRealButton rbLand;
    private RadioRealButton rbRTL;
    private RadioRealButton rbLoiter;
    private RadioRealButton rbAuto;
    int ch1,  ch2, ch3, ch4, ch5, ch6, ch7, ch8, ch9, ch10;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Handler handler = new Handler();

        //Enlazar botones y joysticks con los elementos gráficos
        leftJoystick = (JoystickView)findViewById(R.id.leftJoystick);
        rightJoystick = (JoystickView)findViewById(R.id.rightJoystick);
        rbStabilize = (RadioRealButton)findViewById(R.id.rbStabilize);
        rbHold = (RadioRealButton)findViewById(R.id.rbHold);
        rbLand = (RadioRealButton)findViewById(R.id.rbLand);
        rbRTL = (RadioRealButton)findViewById(R.id.rbRTL);
        rbLoiter = (RadioRealButton)findViewById(R.id.rbLoiter);
        rbAuto = (RadioRealButton)findViewById(R.id.rbAuto);

        //Dar valores por defecto a canales
        ch6= 1500;
        ch7= 1100;
        ch8 = 1500;
        ch9 =1500;
        ch10 =1500;

        //Fijar posicion
        leftJoystick.setAutoReCenterButton(false);

        Runnable communications = new Runnable() {

            @Override
            public void run() {

                //obtener valores de los canales mediante una conversión de la librería
                ch1 = rightJoystick.getNormalizedX() * 10 +1000;
                ch2 = 2000 -10* rightJoystick.getNormalizedY();
                ch3 = 2000 -10* leftJoystick.getNormalizedY();
                ch4 = leftJoystick.getNormalizedX() * 10 +1000;

                //verificar el modo de vuelo por defecto estable
                ch5 =comprobarModo();

                //concatenación de la cadena con los valores de los canales convertidos a string
                String mensaje = String.valueOf(ch1)+String.valueOf(ch2)+String.valueOf(ch3)+String.valueOf(ch4)+ch5+ch6+ch7+ch8+ch9+ch10;
                //Conexión de la app con el módulo mediante UDP
                try {
                    DatagramSocket udpSocket = new DatagramSocket(15793);
                    InetAddress serverAddr = InetAddress.getByName("192.168.4.1");
                    //Enviar la informacion de los canales previa conversión a bytes
                    byte[] buf = mensaje.getBytes();
                    DatagramPacket packet = new DatagramPacket(buf, buf.length, serverAddr, 4210);
                    udpSocket.send(packet);
                } catch (SocketException e) {
                    Log.e("Udp:", "Socket Error:", e);
                } catch (IOException e) {
                    Log.e("Udp Send:", "IO Error:", e);
                }
                Log.i("Buenas","Buenas");
                handler.postDelayed(this, 10);
            }
        };

        handler.post(communications);


    }


    //Obtener modo que el usuario a pulsado
    public int comprobarModo (){


        if (rbStabilize.isChecked()){
            return 1100;
        }
        else if (rbHold.isChecked()){
            return 1200;
        }
        else if (rbLoiter.isChecked()){
            return  1300;
        }
        else if (rbAuto.isChecked()){
            return 1400;
        }
        else if (rbRTL.isChecked()){
            return 1800;
        }
        else if (rbLand.isChecked()){
            return 1900;
        }
        else {
            return 1100;
        }
    }

    //Metodos para implementar el menu

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    //Metodo para saber si se ha pulsado alguna opción del menu, en este caso solo hay boton de apagado
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //Si se pulsa el botón de apagado se abre un dialo y se pide confirmación d ela acción, tras la confirmación se pone el valor 1900 al canal 7
        if (id == R.id.stop) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("¿Quiere detener el dron de forma forzada?").setTitle("Aviso parada");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ch7= 1900;
                    try {
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    }
