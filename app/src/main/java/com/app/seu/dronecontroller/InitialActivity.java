package com.app.seu.dronecontroller;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class InitialActivity extends AppCompatActivity {

    private Button startButton;
    private Button configButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Botones
        startButton = (Button) findViewById(R.id.bStart);
        configButton = (Button) findViewById(R.id.bConfig);

        //Acción al pulsar el botón de play
        //Comprueba que estes conectado a una red wifi y si no es asi aparece un Toast
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isConnectedWifi(v.getContext())){
                    Toast.makeText(getApplicationContext() ,"No estas conectado a ninguna red WiFi",Toast.LENGTH_LONG).show();
                }
                else {
                    //Inicia la actividad
                    Intent intent = new Intent(InitialActivity.this, ControllerActivity.class);
                    startActivity(intent);
                }

            }
        });

        //Acción al pulsar el botón de configuración
        configButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Iniciar la actividad
                Intent intent = new Intent(InitialActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

    }
    //Metodo para comprobar que estas conectado a una red wifi
    public static boolean isConnectedWifi(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        //Character comilla = '"';
        //String red = comilla +"Eustaquio2" + comilla;
        return (networkInfo != null && networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_WIFI);

    }
}





